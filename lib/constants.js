// Write your package code here!
Const = {
    maxUsersPerSession: 10,
    initialUsersPerSession: 2,
    randomCharPool: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
    idLength: 8,
    meetingMarkerLabel: 'Meet me here',
    messages: {
        sessionIsFull: 'The session you were trying to join is full.',
        geolocationNotSupported: 'Your browser does not support geolocation. You are in view-only mode.',
        removedSuccess: 'You have been removed. Refresh the page to re-appear',
        unknownError: 'An unknown error occurred. Refresh the page and try again',
        meetMeMarker: 'Drag the meeting point marker to change it',
        newUserJoined: 'Another person has joined your map',
        instructionHowToUpdate: 'Leave your screen on to update your position'
    },
    sessionVars: {
        userId: 'userId',
        sessionId: 'sessionId',
        isViewOnly: 'isViewOnly',
        isFriend: 'isFriend',
        isFirstUse: 'isFirstUse',
        errorMessage: 'errorMessage',
        successMessage: 'successMessage',
        userData: 'userData',
        userChangedMeeting: 'userChangedMeeting',
        hasLocationPermission: 'hasLocationPermission',
        isInitializing: 'isInitializing'
    },
    methodNames: {
        insertUser: 'insertUser',
        updateUser: 'updateUser',
        updateUserName: 'updateUserName'
    },
    errorTypes: {
        maxUsersReached: 'max-users-reached'
    },
    userTypes: {
        sessionManager: 'sessionManager',
        meetingMarker: 'meetingMarker'
    }
};