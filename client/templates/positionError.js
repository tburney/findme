/**
 * Created by timon on 1/05/15.
 */
Template.positionError.helpers({
    errorMessage: function () {
        var message;
        switch (Template.currentData().errorCode) {

            case 1:
                message = "You have chosen to deny access to your current location.";
                break;
            case 2:
                message = "Your device is not able to send your location. Please check your location settings and try again.";
                break;
            case 3:
                message = "The request for your location has timed out. Please refresh and try again.";
                break;
            default:
                break;

        }
        return message;

    }
});