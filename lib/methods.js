/**
 * Created by timon on 6/05/15.
 */
Meteor.methods({
    updateUser: function (selector, data) {

        Users.update(selector,
            {
                $set: {
                    name: data.name,
                    position: data.position,
                    modifiedDate: new Date()
                }
            });
    },
    updateUserName: function (selector, name) {
        Users.update(selector,
            {
                $set: {
                    name: name,
                    modifiedDate: new Date()
                }
            });
    },
    updateUserPosition: function (selector, position) {

        Users.update(selector,
            {
                $set: {
                    position: position,
                    modifiedDate: new Date()
                }
            });
    },
    insertUser: function (user) {
        var otherUsersInSession = Users.find({
            sessionId: user.sessionId,
            $and: [
                {userId: {$ne: null}}, //meet me here
                {userId: {$ne: user.userId}}
            ]
        }).count();

        if (otherUsersInSession >= 10 && user.userId) {
            throw new Meteor.Error('max-users-reached', 'Maximum number of users per session has been reached');
        }
        Users.upsert(user, {
            $set: {
                userId: user.userId,
                sessionId: user.sessionId,
                position: user.position,
                createdDate: new Date(),
                modifiedDate: new Date()
            }
        }, function (error, numRecordsUpdated) {
            return numRecordsUpdated;
        });
    }

});