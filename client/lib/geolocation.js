// is location refreshing currently on?
var watchingPosition = false;

// current location variable and dependency
var location = new ReactiveVar(null);

// error variable and dependency
var error = new ReactiveVar(null);

var initializing = new ReactiveVar(true);

var watchNum;

// options for watchPosition
var defaultOptions = {
    enableHighAccuracy: true,
    maximumAge: 1
};

var getErrorMessage = function (errorCode) {

    var message;
    switch (errorCode) {

        case 1:
            message = "Permission to access your current location is denied.";
            break;
        case 2:
            message = "Your device is not able to send your location. Please check your location settings and try again.";
            break;
        case 3:
            message = "The request for your location has timed out. Please refresh and try again.";
            break;
        case 4:
            message = "Geolocation is not supported. Try a different browser.";
            break;
        default:
            break;

    }
    return message;
};

var onError = function (newError) {
    Session.set(Const.sessionVars.isInitializing,false);
    error.set(newError);
    Session.set('errorMessage', getErrorMessage(newError.code));
};

var onPosition = function (newLocation) {
    location.set(newLocation);
    var coords = jQuery.extend(true, {}, newLocation.coords);
    Meteor.call('updateUserPosition', Session.get('userData'), {coords: coords});

    error.set(null);
    Session.set(Const.sessionVars.isInitializing,false);

    //we know that the user has allowed access to their permission info
    Session.setDefault('hasLocationPermission', true);
};

var startWatchingPosition = function (customOptions) {
    var options = _.defaults(customOptions || {}, defaultOptions);
    if (!watchingPosition && navigator.geolocation) {
        watchNum = navigator.geolocation.watchPosition(onPosition, onError, options);
        watchingPosition = true;
        Session.set(Const.sessionVars.isInitializing,true);
    }else if (!navigator.geolocation){
         this.onError({code: 4, message: ""});
    }
};

// exports

/**
 * @summary The namespace for all geolocation functions.
 * @namespace
 */
Geolocation = {
    /**
     * @summary Get the current geolocation error
     * @return {PositionError} The
     * [position error](https://developer.mozilla.org/en-US/docs/Web/API/PositionError)
     * that is currently preventing position updates.
     */
    error: function () {
        startWatchingPosition();
        return error.get();
    },

    /**
     * @summary Get the current location
     * @return {Position | null} The
     * [position](https://developer.mozilla.org/en-US/docs/Web/API/Position)
     * that is reported by the device, or null if no position is available.
     */
    currentLocation: function (options) {
        startWatchingPosition(options);
        return location.get();
    },

    stopWatching: function () {
        navigator.geolocation.clearWatch(watchNum);
        watchingPosition = false;
    },
    // simple version of location; just lat and lng

    /**
     * @summary Get the current latitude and longitude
     * @return {Object | null} An object with `lat` and `lng` properties,
     * or null if no position is available.
     */
    latLng: function () {
        var loc = Geolocation.currentLocation();

        if (loc) {
            return {
                lat: loc.coords.latitude,
                lng: loc.coords.longitude
            };
        }

        return null;
    }
};