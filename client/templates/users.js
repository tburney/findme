/**
 * Created by timon on 29/04/15.
 */

Template.users.helpers({
    users: function () {
        return Users.find({sessionId: Session.get('sessionId')});
    }
});