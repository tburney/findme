/**
 * Created by timon on 6/05/15.
 */

Template.locStatusBar.helpers({
    loc: function () {
        if (Template.currentData().position) {
            var accuracy = 100000000;
            var position = JSON.parse(JSON.stringify(Template.currentData().position));
            position.coords.latitude = Math.round(position.coords.latitude * accuracy) / accuracy;
            position.coords.longitude = Math.round(position.coords.longitude * accuracy) / accuracy;
            position.coords.accuracy = Math.round(position.coords.accuracy * 1000) / 1000;
            return position;
        }
    }
});