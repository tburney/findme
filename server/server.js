/**
 * Created by timon on 7/05/15.
 */
if (Meteor.isServer) {
    Meteor.publish('users', function (query, options) {
        if (options) {
            check(options, {
                fields: Match.Optional(Object)
            });
        }
        return Users.find(query, options);
    });

    // Clean up sessions that are older than x amount
    SyncedCron.add({
        name: 'Cleanup sessions',
        schedule: function(parser) {
            // parser is a later.parse object
            return parser.text('every 1 hour');
        },
        job: function() {
            var oneDayAgo = new Date((new Date())-1000*60*60*24);
            Users.remove({createdDate:{$gt: oneDayAgo}});
        }
    });
    SyncedCron.start();
}