if (Meteor.isClient) {

    Template.Home.helpers({
            locError: Geolocation.error,
            showLocError: function () {
                return Geolocation.error();
            },
            sessionIdUrlData: function () {
                return {_sessionId: Session.get(Const.sessionVars.sessionId) || ""};
            },
            user: function () {
                return Users.findOne({userId: Session.get(Const.sessionVars.userId)});
            },
            notViewOnly: function () {
                return !Session.get(Const.sessionVars.isViewOnly);
            },
            isSessionManager: function () {
                return !Session.get(Const.sessionVars.isFriend);
            },
            showMeetingTextBox: function(){
                return Users.find({userType: Const.userTypes.meetingMarker}).count()==1;
            }
            /* ,
             showInfoAlert: function () {
             return !Session.get('hasLocationPermission') && Modernizr.geolocation;
             },
             showCompatibilityAlert: function () {
             return !Modernizr.geolocation;
             }*/

        }
    );


    Template.Home.onCreated(function () {

        var sessionId = Session.get(Const.sessionVars.sessionId);
/*
        Meteor.subscribe('users',
            {sessionId: sessionId},
            {fields: {userId: 1, name: 1, position: 1, userType: 1, createdDate: 1, modifiedDate: 1}});
*/

        if (!Session.get(Const.sessionVars.isFriend) || !Session.get(Const.sessionVars.isViewOnly)) {
            Geolocation.currentLocation();
        }

        if (Modernizr && !Modernizr.geolocation) {
            Session.set(Const.sessionVars.errorMessage, Const.messages.geolocationNotSupported)
        }

    });

    Template.Home.rendered = function () {
        //jQuery document ready stuff goes here

        //initialise the side menu
        var jPM = $.jPanelMenu({
            panel: '#panel',
            clone: false,
            openPosition: '280px'
        });
        jPM.on();

        if (!Session.get(Const.sessionVars.isFriend) && Session.get(Const.sessionVars.isFirstUse)) {
            $('#sendLinkCard').show();
        }
        if (Session.get(Const.sessionVars.isFriend) && Session.get(Const.sessionVars.isFirstUse)) {
            $('#firstUseCard').show();
        }


        Tracker.autorun(function () {
            var errorMessage = Session.get(Const.sessionVars.errorMessage);
            if (errorMessage) {
                Materialize.toast(Session.get(Const.sessionVars.errorMessage), 4000, 'red lighten-3');
            }
        });

        Tracker.autorun(function () {
            var successMessage = Session.get(Const.sessionVars.successMessage);
            if (successMessage) {
                Materialize.toast(Session.get(Const.sessionVars.successMessage), 4000, 'green lighten-2');
            }
        });

        Tracker.autorun(function(){
            var allowLocation= Session.get(Const.sessionVars.hasLocationPermission);
            var allowSwitch = $('[name="allowLocation"]');
            if(allowLocation){
                allowSwitch.val('on');
            }else{
                allowSwitch.val('off');
            }
        });

        // Show the initialize location toast whilst browser is getting the location
        Tracker.autorun(function () {
            var isInitializing = Session.get(Const.sessionVars.isInitializing);
            if (isInitializing) {
                $('#statusBar').html('Waiting for Location...')
            }else{
                $('#statusBar').html('')
            }
        });

    };

    Template.Home.events({
        'click #deleteMe': function (event) {
            Meteor.call(Const.methodNames.updateUser, Session.get(Const.sessionVars.userData), {
                name: null,
                position: null
            }, function (error, result) {
                if (result) {
                    Session.set(Const.sessionVars.successMessage, Const.messages.removedSuccess);
                }
                if (error) {
                    Session.set(Const.sessionVars.errorMessage, Const.messages.unknownError);
                }
            });
            Geolocation.stopWatching();

        },
        'blur #name': function (event) {
            Meteor.call(Const.methodNames.updateUserName, Session.get(Const.sessionVars.userData), event.target.value);
        },
        'keyup #name': function (event) {
            if (event.keyCode == 13 || event.keyCode == 27) {//enter or esc
                event.target.blur();
            }
        },
        'blur #meetMeMessage': function (event) {
            var message = $.trim(event.target.value);
            if (message) {
                message = message.substr(0, 255);
                Meteor.call(Const.methodNames.updateUserName, {userType: Const.userTypes.meetingMarker}, message, function(error,result){
                    if(error){
                        console.log(error.message);
                    }
                });
            }
        },
        'keyup #meetMeMessage': function (event) {
            if (event.keyCode == 13 || event.keyCode == 27) {//enter or esc
                event.target.blur();
            }
        },
        'change [name="accuracy"]': function (event) {
            var enableHighAccuracy = true;

            if (event.target.value != 'on') {
                enableHighAccuracy = false;
            }
            var options = {enableHighAccuracy: enableHighAccuracy};
            Geolocation.stopWatching();
            Geolocation.currentLocation(options);
        },
        'change [name="allowLocation"]': function (event) {
            var allowLocation = event.target.value == 'on';
            if(allowLocation){
                Geolocation.currentLocation();
            }else{
                Geolocation.stopWatching();
            }
        },
        'click #title': function (event) {
            event.preventDefault();
            window.location = '/';
        },
        'click #closeLinkCard': function (event) {
            Session.set(Const.sessionVars.isFirstUse, false);
            Materialize.toast(Const.messages.instructionHowToUpdate, 6000, 'orange lighten-1');
            $('#sendLinkCard').hide();
        },
        'click #refreshPanZoom': function (event) {
            refreshPanZoom();
        },
        'click #firstUseAgree': function (event) {
            $('#firstUseCard').hide();
            Session.set(Const.sessionVars.isFirstUse, false);
            Session.set(Const.sessionVars.isViewOnly, false);
            Geolocation.currentLocation();
        },
        'click #firstUseCancel': function (event) {
            Session.set(Const.sessionVars.isFirstUse, false);
            $('#firstUseCard').hide();

        }
    });


}