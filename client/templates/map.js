if (Meteor.isClient) {
    var mapGlobal;
    var markers = {};
    var infoWindows = {};

    Meteor.startup(function () {
        GoogleMaps.load({v: '3', key: 'AIzaSyAr9NyDMeZL5spkzZc6Rr-s1vTfIwFqoQU'});
        //GoogleMaps.loadUtilityLibrary('https://cdn.rawgit.com/denissellu/routeboxer/master/src/RouteBoxer.js')
    });

    Template.map.onCreated(function () {
        GoogleMaps.ready('map', function (map) {
            mapGlobal = map.instance;

            // google.maps.event.addListener(mapGlobal, 'click', function (event) {
            //     var position = {coords: {latitude: event.latLng.lat(), longitude: event.latLng.lng()}};
            //     var meetingMarker = Users.findOne({userType: Const.userTypes.meetingMarker});
            //     if (!meetingMarker) {
            //         Meteor.call(Const.methodNames.insertUser, {
            //             sessionId: Session.get(Const.sessionVars.sessionId),
            //             name: Const.meetingMarkerLabel,
            //             position: position,
            //             userType: Const.userTypes.meetingMarker
            //         });
            //     }
            // });


            addUserMarkers();
            //addSearchBox();

        });
    });

    Template.map.helpers({
        mapOptions: function () {

            if (GoogleMaps.loaded()) {
                var user = Users.findOne({userId: Session.get(Const.sessionVars.userId), position: {$exists: true}});
                var center, zoom;
                if (user && user.position && user.position.coords) {
                    //get position from user geolocation
                    center = new google.maps.LatLng(user.position.coords.latitude, user.position.coords.longitude);
                    zoom = 15;
                }
                else {
                    //default to world view
                    center = new google.maps.LatLng(0, 0);
                    zoom = 3;
                }
                return {
                    center: center,
                    zoom: zoom,
                    disableDefaultUI: true
                };

            }
        }
    });


    refreshPanZoom = function () {

        var bounds = new google.maps.LatLngBounds();

        _.each(markers, function (marker) {
            bounds.extend(marker.getPosition());
        });

        mapGlobal.fitBounds(bounds);
        mapGlobal.panToBounds(bounds);
        if (mapGlobal.getZoom() > 18) {
            mapGlobal.setZoom(18);
        }

    };

    function addInfoWindow(marker, document) {
        var infowindow = new google.maps.InfoWindow({
            content: document.name || '?'
        });

        infowindow.open(mapGlobal, marker);
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(mapGlobal, marker);
        });

        return infowindow;
    }

    function createAccuracyCircle(mapPosition, accuracy) {
        return new google.maps.Circle({
            center: mapPosition,
            radius: accuracy,
            map: mapGlobal,
            fillColor: '#4285F4',
            fillOpacity: 0.15,
            strokeColor: '#4285F4',
            strokeOpacity: 0

        });
    }

    function addUserMarkers() {
        var sessionId = Session.get(Const.sessionVars.sessionId);

        var accuracyCircles = {};

        Users.find({}).observe({ //users collection is only subscribed to users in the session
            added: function (user) {
                addUserMarker(user);

            },
            changed: function (newUser, oldUser) {

                var position = newUser.position;

                //user has clicked the delete me button and wiped their position object
                if (!position && markers[newUser._id]) {

                    markers[oldUser._id].setMap(null);
                    accuracyCircles[oldUser._id].setMap(null);
                    infoWindows[oldUser._id].setMap(null);
                    google.maps.event.clearInstanceListeners(markers[oldUser._id]);
                    delete markers[oldUser._id];
                    delete accuracyCircles[oldUser._id];
                    delete infoWindows[oldUser._id];
                    return;
                }

                if (position && !markers[newUser._id]) {
                    //marker hasn't been added yet
                    addUserMarker(newUser);
                    return;
                }

                if (!markers[newUser._id] && !position) {
                    //user hasn't shared their location yet
                    return;
                }

                if (newUser.position != oldUser.position) {
                    //if position has changed  i.e. name could have changed without position changing
                    var newPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    markers[newUser._id].setPosition(newPosition);
                    accuracyCircles[newUser._id].setCenter(newPosition);
                }

                var accuracy = position.coords.accuracy;
                if (accuracy && accuracy != accuracyCircles[newUser._id].getRadius()) {
                    accuracyCircles[newUser._id].setRadius(accuracy);
                }

                //update the name of the user marker
                if (newUser.name != oldUser.name) {
                    if (newUser.userType == Const.userTypes.meetingMarker) {
                        newUser.name = '<strong>' + Const.meetingMarkerLabel + '</strong><br/>'
                            + '<i>' + newUser.name + '</i>';
                    }
                    infoWindows[newUser._id].setContent(newUser.name);
                    infoWindows[newUser._id].open(mapGlobal, markers[newUser._id]);


                }

                if (newUser.userType == Const.userTypes.meetingMarker) {
                    if (!Session.get(Const.sessionVars.userChangedMeeting)) {
                        refreshPanZoom();

                    } else {
                        Session.set(Const.sessionVars.userChangedMeeting, false);
                    }
                }

                //calcRoute();
            }
            ,
            removed: function (oldDocument) {
                if (markers[oldDocument._id]) {
                    markers[oldDocument._id].setMap(null);
                    google.maps.event.clearInstanceListeners(markers[oldDocument._id]);
                    delete markers[oldDocument._id];
                }
            }

        });

        function addUserMarker(document) {

            var position = document.position;
            if (!position || !position.coords) {
                return;
            }
            var mapPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

            var marker = new google.maps.Marker({
                draggable: false,
                animation: google.maps.Animation.DROP,
                position: mapPosition,
                map: mapGlobal,
                id: document._id,
                title: document.name
            });
            if (document.userType == Const.userTypes.meetingMarker) {
                return;
                marker.setIcon('https://maps.google.com/mapfiles/ms/icons/green-dot.png');
                marker.setDraggable(true);

                google.maps.event.addListener(marker, 'dragend', function (event) {
                    marker.setOptions({lat: event.latLng.lat(), lng: event.latLng.lng()});
                    var position = {coords: {latitude: event.latLng.lat(), longitude: event.latLng.lng()}};
                    Session.set(Const.sessionVars.userChangedMeeting, true);
                    Meteor.call('updateUserPosition', {
                        sessionId: sessionId,
                        userType: Const.userTypes.meetingMarker
                    }, position);
                });

                Session.set(Const.sessionVars.successMessage, Const.messages.meetMeMarker);
            }

            if (document.userId == Session.get('userId')) {
                //current user only
                marker.setIcon('https://maps.google.com/mapfiles/ms/icons/blue-dot.png');
            } else if (!Session.get(Const.sessionVars.isFirstUse) && document.userType != Const.userTypes.meetingMarker) {
                Session.set(Const.sessionVars.successMessage, Const.messages.newUserJoined);
            }

            accuracyCircles[document._id] = createAccuracyCircle(mapPosition, position.coords.accuracy);


            markers[document._id] = marker;
            infoWindows[document._id] = addInfoWindow(marker, document);
            if (document.userType != Const.userTypes.meetingMarker) {
                //destination label will already be visible and it messes up click off place search bounds
                refreshPanZoom();
            }

        }
    }


    function addSearchBox() {
        var placeMarkers = [];
        var map = mapGlobal;
        /*var defaultBounds = new google.maps.LatLngBounds(
         new google.maps.LatLng(-33.8902, 151.1759),
         new google.maps.LatLng(-33.8474, 151.2631));*/
        map.fitBounds(mapGlobal.getBounds());

        // Create the search box and link it to the UI element.
        var input = /** @type {HTMLInputElement} */(
            document.getElementById('placeSearch'));
        //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        var searchBox = new google.maps.places.SearchBox(
            /** @type {HTMLInputElement} */(input));

        // Listen for the event fired when the user selects an item from the
        // pick list. Retrieve the matching places for that item.
        google.maps.event.addListener(searchBox, 'places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }
            for (var i = 0, marker; marker = placeMarkers[i]; i++) {
                marker.setMap(null);
            }

            // For each place, get the icon, place name, and location.
            placeMarkers = [];
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0, place; place = places[i]; i++) {
                var image = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                var marker = new google.maps.Marker({
                    map: map,
                    icon: image,
                    title: place.name,
                    position: place.geometry.location
                });

                placeMarkers.push(marker);

                bounds.extend(place.geometry.location);
            }

            _.each(markers, function (marker) {
                bounds.extend(marker.getPosition());
            });
            map.fitBounds(bounds);
        });

        // Bias the SearchBox results towards places that are within the bounds of the
        // current map's viewport.
        google.maps.event.addListener(map, 'bounds_changed', function () {
            var bounds = map.getBounds();
            searchBox.setBounds(bounds);
        });
    }

    function calcRoute() {

        var users = Users.find({userType: {$ne: Const.userTypes.meetingMarker}}).fetch();

        if (users.length != 2) {
            return
        }

        if (!users[0].position || !users[1].position) {
            return;
        }
        var routeBoxer = new RouteBoxer();

        var directionService = new google.maps.DirectionsService();
        var directionsRenderer = new google.maps.DirectionsRenderer({map: mapGlobal});

        var origin = new google.maps.LatLng(users[0].position.coords.latitude, users[0].position.coords.longitude);
        var destination = new google.maps.LatLng(users[1].position.coords.latitude, users[1].position.coords.longitude);
        var request = {
            origin: origin,
            destination: destination,
            travelMode: google.maps.TravelMode.DRIVING
        };
        directionService.route(request, function (result, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsRenderer.setDirections(result);

                // Box around the overview path of the first route
                var path = result.routes[0].overview_path;
                //var boxes = routeBoxer.box(path, distance);

            } else {
                Session.set(Const.sessionVars.errorMessage, "Couldn't find directions");
            }

            /*for (var i = 0; i < boxes.length; i++) {
             var bounds = box[i];
             // Perform search over this bounds
             }*/
        });

    }
}
