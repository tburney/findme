Router.route('/', function () {
    var data = {_sessionId: generateId(), _userId: generateId()};
    this.redirect('session.user', data);
}, {
    name: 'home'
});

Router.route('/:_sessionId', function () {
    var sessionId = this.params._sessionId;
    var userId = generateId();
    Session.set(Const.sessionVars.isViewOnly, true);
    Session.set(Const.sessionVars.isFriend, true);
    var data = {_sessionId: sessionId, _userId: userId};
    this.redirect('session.user', data);
}, {
    name: 'session'
});

Router.route('/:_sessionId/:_userId', function () {
    GAnalytics.pageview("/");
    Session.set(Const.sessionVars.isFirstUse, true);
    this.wait(Meteor.subscribe('users',
        {sessionId: this.params._sessionId},
        {fields: {userId: 1, name: 1, position: 1, userType: 1, createdDate: 1, modifiedDate: 1}}));
    var thisRoute = this;
    Meteor.call(Const.methodNames.insertUser, {
        sessionId: this.params._sessionId,
        userId: this.params._userId
    }, function (error, result) {
        if (error && error.error === Const.errorTypes.maxUsersReached) {
            Session.keys = {};
            Session.set(Const.sessionVars.errorMessage, Const.messages.sessionIsFull);
            thisRoute.redirect('home');
        }

    });
    if (this.ready()) {
        Session.set('userId', this.params._userId);
        Session.set('sessionId', this.params._sessionId);
        Session.set('userData', {userId: this.params._userId, sessionId: this.params._sessionId});
        this.render("Home");
    } else {
        this.render("Loading");
    }

}, {
    name: 'session.user'
});


var generateId = function () {
    return chance.string({length: Const.idLength, pool: Const.randomCharPool});
}